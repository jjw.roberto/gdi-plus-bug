unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, GDIPAPI, GDIPOBJ, GDIPUTIL;

type
  TForm1 = class(TForm)
    PaintBox1: TPaintBox;
    procedure PaintBox1Paint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
{$R jjwfont.res}

function MakeGDIPColor(C: TColor; Alpha: Byte): Cardinal;
var
  tmpRGB : TColorRef;
begin
  tmpRGB := ColorToRGB(C);

  result := ((DWORD(GetBValue(tmpRGB)) shl  BlueShift) or
             (DWORD(GetGValue(tmpRGB)) shl GreenShift) or
             (DWORD(GetRValue(tmpRGB)) shl   RedShift) or
             (DWORD(Alpha) shl AlphaShift));
end;


var
  // vari�veis globais para utiliza��o da fonte da JJW com os glyph's
  gFontCollectionJJW: TGPPrivateFontCollection=nil;
  gFontFamilyJJW: TGPFontFamily=nil;

function GetJJWFont(out aFontCollection: TGPPrivateFontCollection; out aFontFamily: TGPFontFamily): boolean;
const
  // nome do resource aonda est� embarcada a fonte
  RES_NAME = 'JJW_FONT';
  // tipo do resource da fonte
  RES_TYPE = 'TTF';
  // nome da fonte
  FONT_NAME = 'JJW';
var
  st: Status;
begin
  aFontCollection := TGPPrivateFontCollection.Create;
  try
  with TResourceStream.Create(HInstance, RES_NAME, RES_TYPE) do
    try
      st := aFontCollection.AddMemoryFont(Memory, Size);
    finally
      Free;
    end;

    Result := st = ok;
    if Result then
      aFontFamily := TGPFontFamily.Create(FONT_NAME, aFontCollection)
    else
    begin
      FreeAndNil(aFontCollection);
      aFontFamily := nil;
    end;
  except
    FreeAndNil(gFontCollectionJJW);
    raise;
  end;
end;

procedure TForm1.PaintBox1Paint(Sender: TObject);
const
  JJW_LOGO_CHAR = 'W';
var
  fc: TGPPrivateFontCollection;
  ff: TGPFontFamily;
  f: TGPFont;
  b: TGPBrush;
  r, tx: TGPRectF;
  txFmt: TGPStringFormat;
  gp: TGPGraphicsPath;
  pgb: TGPPathGradientBrush;
  colors: ARGB;
  cnt: integer;
begin
  with TGPGraphics.Create(PaintBox1.Canvas.Handle) do
    try
      r.X := 0;
      r.Y := 0;
      r.Width := PaintBox1.Width;
      r.Height := PaintBox1.Height;

      // FUNDO COM GRADIENTE REDONDO

      colors := MakeGDIPColor(RGB(11, 141, 221), 255);
      cnt := 1;

      // pinta o fundo todo primeiro
      b := TGPSolidBrush.Create(colors);
      try
        FillRectangle(b, r);
      finally
        b.free;
      end;

      // calcula um quadrado no centro
      tx := r;
      if tx.Width > tx.Height then
        tx.Width := tx.Height
      else
        tx.Height := tx.Width;

      tx.X := (r.Width / 2) - (tx.Width / 2);
      tx.Y := (r.Height / 2) - (tx.Height / 2);

      // pinta o gradiente redondo
      gp := TGPGraphicsPath.Create();
      try
        gp.AddEllipse(tx);

        pgb := TGPPathGradientBrush.Create(gp);
        try
          pgb.SetCenterColor(MakeGDIPColor(RGB(101, 174, 216), 255));
          pgb.SetSurroundColors(@colors, cnt);
          FillPath(pgb, gp);
        finally
          pgb.Free;
        end;
      finally
        gp.free;
      end;

      if GetJJWFont(fc, ff) then
      begin
        f := TGPFont.Create(ff, (r.Height * 0.13));
        b := TGPSolidBrush.Create(MakeGDIPColor(RGB(28,133,192), 255));
        txFmt := TGPStringFormat.Create();
        try
          SetTextRenderingHint(TextRenderingHintAntiAlias);

          MeasureString(JJW_LOGO_CHAR, 1, f, r, tx);

          tx.X := (r.Width / 2)  - (tx.Width / 2);
          tx.Y := (r.Height / 2) - (tx.Height / 2);

          DrawString(JJW_LOGO_CHAR, 1, f, tx, txFmt, b);
        finally
          txFmt.Free;
          b.Free;
          f.Free;
          ff.free;
          fc.free;
        end;
      end;
    finally
      Free;
    end;
end;

end.
